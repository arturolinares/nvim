-- Read the docs: https://www.lunarvim.org/docs/configuration
-- Video Tutorials: https://www.youtube.com/watch?v=sFA9kX-Ud_c&list=PLhoH5vyxr6QqGu0i7tt_XoVK9v-KvZ3m6
-- Forum: https://www.reddit.com/r/lunarvim/
-- Discord: https://discord.com/invite/Xb9B4Ny
-- reload('plugin/user/lsp.lua')

reload('user.options');
reload('user.plugins');
reload('user.colorscheme');
reload('user.keymaps');
reload('user.debug');
reload('user.which_keys');
reload('user.lsp');
reload('user.telescope');
