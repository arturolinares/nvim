local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

local opts = {
  capabilities = capabilities,
  filetypes = { "twig", "html", "erb", "blade", "vue" },
}
require("lvim.lsp.manager").setup("emmet_ls", opts)
require("lvim.lsp.manager").setup("twiggy-language-server", {
  filetypes = { "twig" },
})
