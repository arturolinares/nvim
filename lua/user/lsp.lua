------------------------
-- LSP
------------------------
local lsp_manager = require("lvim.lsp.manager")

vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, {
  "intelephense",
})
-- lvim.builtin.lspconfig.inlay_hints.enabled = true
-- vim.tbl_map(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

lsp_manager.setup('phpactor', {
  -- cmd = { "/usr/local/bin/phpactor", "language-server", "-vvv" },
  command = "phpactor",
  filetypes = { "php" },
  root_pattern = {".phpactor.json", "composer.json", ".git" },
});
lsp_manager.setup('marksman');

local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  -- {
  --   command = "phpcbf",
  --   filetypes = { "php" },
  --   args = { "--standard=Drupal,DrupalPractice", "--extensions=php,module,inc,install,theme" },
  -- },
  {
    name = "prettier",
    ---@usage arguments to pass to the formatter
    -- these cannot contain whitespace
    -- options such as `--line-width 80` become either `{"--line-width", "80"}` or `{"--line-width=80"}`
    args = { "--print-width", "100" },
    ---@usage only start in these filetypes, by default it will attach to all filetypes it supports
    filetypes = { "typescript", "typescriptreact", "css" },
  },
}
-- DEBUG
-- lvim.lsp.null_ls.setup.debug = true
-- LSP Log level
-- vim.lsp.set_log_level "debug"

local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
  -- PHP Actor integrates now phpcs and phpstan. Keeping this commented in case
  -- in the future I try intelephense.
  -- {
  --   command = "phpcs",
  --   filetypes = { "php" },
  --   args = { "--standard=Drupal,DrupalPractice", "--extensions=php,inc,module,install,theme" }
  -- },
  -- {
  --   command = "phpstan",
  --   filetypes = { "php" },
  --   args = {
  --     "--level=4",
  --     "--memory-limit=2G",
  --     -- "--configuration=phpstan.neon",
  --    -- "--autoload-file=vendor/autoload.php"
  --   }
  -- },
  -- erb-lint
  {
    command = "erb-lint",
    filetypes = { "erb" },
    args = { "--auto-correct" }
  },
}
-- Copilot
-- table.insert(lvim.plugins, {
--   "zbirenbaum/copilot-cmp",
--   event = "InsertEnter",
--   dependencies = { "zbirenbaum/copilot.lua" },
--   config = function()
--     vim.defer_fn(function()
--       require("copilot").setup()     -- https://github.com/zbirenbaum/copilot.lua/blob/master/README.md#setup-and-configuration
--       require("copilot_cmp").setup() -- https://github.com/zbirenbaum/copilot-cmp/blob/master/README.md#configuration
--     end, 100)
--   end,
-- })



