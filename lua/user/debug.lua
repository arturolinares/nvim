local dap = require("dap")
local mason_path = vim.fn.glob(vim.fn.stdpath("data") .. "/mason/")


require('dap.ext.vscode').load_launchjs();

dap.adapters.php = {
  type = "executable",
  command = "node",
  args = { mason_path .. "packages/php-debug-adapter/extension/out/phpDebug.js" },
}

dap.adapters["pwa-node"] = {
  type = "server",
  host = "localhost",
  port = "${port}",
  executable = {
    command = "node",
    args = { mason_path .. "packages/js-debug-adapter/js-debug/src/dapDebugServer.js", "${port}" },
  },
}

dap.configurations.javascript = {
  {
    type = "pwa-node",
    request = "launch",
    name = "Launch file",
    program = "${file}",
    cwd = "${workspaceFolder}",
  },
  -- Configure javascript debuggers to attach
  {
    type = "pwa-node",
    request = "attach",
    name = "Attach to Process",
    processId = "${command:PickProcess}",
    cwd = "${workspaceFolder}",
  }
}

dap.adapters.ruby = {
  type = 'executable',
  command = 'bundle',
  args = { 'exec', 'readapt', 'stdio' },
}
-- @info  Add this to Gemfile: gem 'readapt', group: :development
dap.configurations.ruby = {
  {
    type = "ruby",
    request = "launch",
    name = "Rails",
    program = "${workspaceFolder}/bin/rails",
    programArgs = { "s" },
    useBundler = true
  },
  {
    type = 'ruby',
    request = 'launch',
    name = 'Active file',
    program = '${file}',
    programArgs = {},
    useBundler = false,
  },
  {
    type = "ruby",
    request = "launch",
    name = "RSpec (Active File)",
    program = "rspec",
    programArgs = {
      "-I",
      "${workspaceFolder}",
      "${file}"
    },
    useBundler = false
  },
}
dap.adapters.sh = {
  type = "executable",
  command = mason_path .. "bin/bash-debug-adapter",
}
dap.configurations.sh = {
  {
    name = "Launch Bash debugger",
    type = "sh",
    request = "launch",
    program = "${file}",
    cwd = "${fileDirname}",
    pathBashdb = mason_path .. "packages/bash-debug-adapter/extension/bashdb_dir/bashdb",
    pathBashdbLib = mason_path .. "packages/bash-debug-adapter/extension/bashdb_dir/",
    pathBash = "bash",
    pathCat = "cat",
    pathMkfifo = "mkfifo",
    pathPkill = "pkill",
    env = {},
    args = {},
    showDebugOutput = true,
    -- trace = true,
  }
}

-- These are read now using the VS Code launch.json.
-- dap.configurations.php = {
--   {
--     name = "Listen for Xdebug",
--     type = "php",
--     request = "launch",
--     port = 9003,
--     localSourceRoot = '/home/alinares/projects/iasedu2/',
--     serverSourceRoot = '/app',
--   },
--   {
--     name = "Debug currently open script",
--     type = "php",
--     request = "launch",
--     port = 9003,
--     cwd = "${fileDirname}",
--     program = "${file}",
--     runtimeExecutable = "php",
--   },
-- }

-- dap.configurations.
