lvim.builtin.which_key.mappings["gS"] = {
  "<cmd>Git<CR>", "Git Status"
}
lvim.builtin.which_key.mappings["dT"] = {
  "<cmd>dT<CR>", "Clear breakpoints"
}
lvim.builtin.which_key.mappings["dB"] = "Breakpoints to Quickfix ([q, ]q)"
-- lvim.builtin.mappings["gg"] = {
--   "<cmd>LazyGit<CR>", "LazyGit"
-- }
-- lvim.builtin.mappings["<leader>u"] = {
--   "<cmd>vim.cmd.UndotreeToggle<CR>"
-- }

local attempt = require("attempt");

lvim.builtin.which_key.mappings["a"] = {
  name = "Scratchpad (Attempts)",
  n = { attempt.new_select, "New Attempt" },                      -- new attempt, selecting extension
  i = { attempt.new_input_ext, "New Attempt (input extension)" }, -- new attempt, inputing extension
  r = { attempt.run, "Run Attempt" },                             -- run attempt
  d = { attempt.delete_buf, "Delete Attempt" },                   -- delete attempt from current buffer
  c = { attempt.rename_buf, "Rename Attempt" },                   -- rename attempt from current buffer
  l = { 'Telescope attempt', "Search attempts" }                  -- search through attempts
}

local neotest = require("neotest");

lvim.builtin.which_key.mappings["t"] = {
  name = "Tests",
  t = { '<cmd>Neotest run<CR>', "Test closest" },
  l = { '<cmd>Neotest run last<CR>', "Test last" },
  f = { '<cmd>lua require("neotest").run.run("%")<CR>', "Test File" },
  D = { '<cmd>lua require("neotest").run.run("%:p:h")<CR>', "Test Dir" },
  S = { '<cmd>lua require("neotest").run.run({ suite = true })<CR>', "Test Suite" },
  -- w = { '<cmd>lua require("neotest").watch.watch()<CR>', "Watch" },
  o = { '<cmd>lua require("neotest").output.output()<CR>', "Output" },
  s = { '<cmd>lua require("neotest").summary.toggle()<CR>', "Summary" },
  j = { '<cmd>Neotest jump next<CR>', "Jump next" },
  k = { '<cmd>Neotest jump prev<CR>', "Jump prev" },
}
