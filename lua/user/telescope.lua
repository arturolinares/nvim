lvim.builtin.telescope.on_config_done = function(telescope)
  pcall(telescope.load_extension, "aerial")
  pcall(telescope.load_extension, "scope")
  pcall(telescope.load_extension, "refactoring")
  pcall(telescope.load_extension, "attempt")
  pcall(telescope.load_extension, "telescope-alternate")
  pcall(telescope.load_extension, "planets")
  pcall(telescope.load_extension, "projections")
  pcall(telescope.load_extension, "find_template")
  pcall(telescope.load_extension, "ui-select")
end

lvim.builtin.telescope.extensions.aerial = {
  show_nesting = {
    ['_'] = false,
  }
}

vim.table.insert(lvim.builtin.telescope.defaults.vimgrep_arguments, "--no-ignore-vcs")

