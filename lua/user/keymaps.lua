lvim.keys.normal_mode['<leader>pv'] = '<cmd>Ex<CR>';
lvim.keys.normal_mode['<leader>,'] = require('telescope.builtin').git_files;
lvim.keys.normal_mode['<leader>.'] = '<cmd>Telescope find_files no_ignore=true<CR>';
lvim.keys.normal_mode['<leader>o'] = '<cmd>Telescope projections<CR>'
lvim.keys.normal_mode['<leader>lA'] = '<cmd>PhpActor context_menu<CR>'

-- Harpoon
--local mark = require("harpoon.mark")
-- local ui = require("harpoon.ui")

-- lvim.keys.normal_mode['<leader>ma'] = require('harpoon.mark').add_file;
-- lvim.keys.normal_mode['<C-m>'] = ui.toggle_quick_menu;
-- lvim.keys.normal_mode['<C-,>'] = function() ui.nav_file(1) end
-- lvim.keys.normal_mode['<C-.>'] = function() ui.nav_file(2) end
-- lvim.keys.normal_mode['<C-->'] = function() ui.nav_file(3) end

lvim.keys.insert_mode['<A-.>'] = function()
  local cmp = require("lvim.utils.modules").require_on_index "cmp"
  cmp.complete()
end
-- Undotree
vim.keymap.set('n', '<leader>u', vim.cmd.UndotreeToggle)

-- Fugitive
lvim.keys.normal_mode['<leader>gS'] = '<cmd>Git<CR>';

-- Aerial
lvim.keys.normal_mode['gm'] = '<cmd>Telescope aerial<CR>';

-- Buffer Navigation
lvim.keys.normal_mode['<Tab>'] = '<cmd>BufferLineCycleNext<CR>';
lvim.keys.normal_mode['<S-Tab>'] = '<cmd>BufferLineCyclePrev<CR>';

lvim.keys.normal_mode['<leader>bf'] = '<cmd>Telescope buffers<CR>'

-- Refactor
lvim.keys.visual_mode['<leader>r'] = "<Esc><cmd>lua require('telescope').extensions.refactoring.refactors()<CR>";

-- LSP
lvim.keys.normal_mode['<leader>lR'] = "<cmd>LspRestart<CR>"
lvim.keys.normal_mode['<leader>lh'] = "<cmd>lua vim.lsp.buf.hover()<CR>"
lvim.keys.normal_mode['<leader>lD'] = "<cmd>ToggleDiag<CR>"
-- Lazygit using a plugin
lvim.keys.normal_mode['<leader>gg'] = "<cmd>LazyGit<CR>"

-- lvim.keys.normal_mode['<Alt--'] = "`."
--
-- Move selected lines
lvim.keys.visual_mode["J"] = ":m '>+1<CR>gv=gv"
lvim.keys.visual_mode["K"] = ":m '<-2<CR>gv=gv"

-- Join lines without moving the cursor
lvim.keys.normal_mode["J"] = "mzJ`z";

-- Keep the cursor at the middle when moving half a page
lvim.keys.normal_mode["<C-d>"] = "<C-d>zz"
lvim.keys.normal_mode["<C-u>"] = "<C-u>zz"

-- Close all bufers except the current one
lvim.keys.normal_mode["<leader>bc"] = "<cmd>w | %bd | e#<CR>"

lvim.keys.normal_mode['n'] = 'nzzzv'

-- paste over text without losing your copied text
-- lvim.keys.normal_mode['<leader>p'] = "\"_dP"
vim.keymap.set('x', '<leader>p', "\"_dP")

-- use leader + y to copy tp the system clipboard
lvim.keys.normal_mode['<leader>y'] = '"+y'

-- Sloppy fingers saving and exiting vim. Oh, the memes.
vim.api.nvim_create_user_command('W', ':w', { nargs = 0 })
vim.api.nvim_create_user_command('Q', ':q', { nargs = 0 })
vim.api.nvim_create_user_command('Wq', ':wq', { nargs = 0 })

-- use ctrl+, to go back to the position of the last edit
-- ctrl+. to go forward
-- lvim.keys.normal_mode["g,"]= '<Cmd>lua vim.cmd("normal! `[\")<CR>'
-- lvim.keys.normal_mode["g."] = '<Cmd>lua vim.cmd("normal! `]\")<CR>'

-- Atempts

local attempt = require("attempt");
lvim.keys.normal_mode['<leader>an'] = function() attempt.new_select() end    -- new attempt, selecting extension
lvim.keys.normal_mode['<]eader>ai'] = function() attempt.new_input_ext() end -- new attempt, inputing extension
lvim.keys.normal_mode['<leader>ar'] = function() attempt.run() end           -- run attempt
lvim.keys.normal_mode['<leader>ad'] = function() attempt.delete_buf() end    -- delete attempt from current buff]r
lvim.keys.normal_mode['<leader>ac'] = function() attempt.rename_buf() end    -- rename attempt from cur]ent buffer
lvim.keys.normal_mode['<leader>aa'] = '<cmd>Telescope attempt<CR>'           -- search through attempts

-- CtrlSF
lvim.keys.visual_mode['<leader>sF'] = '<Plug>CtrlSFVwordPath'
-- Todo lsit
lvim.keys.normal_mode['<leader>sD'] = '<cmd>TodoTelescope keywords=TODO,@todo,NOTE,HACK<CR>'

-- Symbols pane
lvim.keys.normal_mode['<leader>kM'] = '<cmd>SymbolsOutline<CR>'
lvim.keys.normal_mode['<leader>km'] = '<cmd>AerialOpen<CR>'

-- search in folder
local function grep_in(node)
  if not node then
    return
  end
  local path = node.absolute_path or vim.loop.cwd()
  if node.type ~= 'directory' and node.parent then
    path = node.parent.absolute_path
  end
  if path ~= nil then
    require('telescope.builtin').live_grep({
      search_dirs = { path },
      prompt_title = string.format('Grep in [%s]', vim.fs.basename(path)),
    })
  end
end

lvim.keys.normal_mode['<leader>sT'] = function()
  local node = require('nvim-tree.lib').get_node_at_cursor()
  grep_in(node)
end

lvim.keys.normal_mode['<leader>R'] = '<cmd>Telescope telescope-alternate alternate_file<CR>'

-- Change cwd to cwd
local function change_root_to_global_cwd()
  local api = require("nvim-tree.api")
  local global_cwd = vim.fn.getcwd(-1, -1)
  api.tree.change_root(global_cwd)
  print('New root: ' .. global_cwd)
end
lvim.keys.normal_mode['<C-c>'] = change_root_to_global_cwd;

-- open git commit in browser
lvim.keys.normal_mode['<leader>gL'] = '<cmd>GitBlameOpenCommitURL<CR>'
--
-- hierarchy
lvim.keys.normal_mode['<leader>P'] = function()
  local hierarchy = require('hierarchy')
  local jump_first = hierarchy.handlers.jump_first
  hierarchy.supertypes(jump_first)
end
lvim.keys.normal_mode['<leader>T'] = "<cmd>lua require'hierarchy'.supertypes(require'hierarchy.handlers'.quickfix)<CR>"
lvim.keys.normal_mode['<leader>dB'] = function()
  require('dap').list_breakpoints();
  require("telescope.builtin").quickfix();
end
lvim.keys.normal_mode['<leader>dl'] = '<cmd>lua require("dapui").eval()<CR>'

lvim.keys.normal_mode['<leader>ii'] = '<cmd>Telescope find_template type=insert<CR>'
lvim.keys.normal_mode['<leader>dT'] = function()
  require('dap').clear_breakpoints();
end

function compare_to_clipboard()
  local ftype = vim.api.nvim_eval("&filetype")
  vim.cmd("vsplit")
  vim.cmd("enew")
  vim.cmd("normal! P")
  vim.cmd("setlocal buftype=nowrite")
  vim.cmd("set filetype=" .. ftype)
  vim.cmd("diffthis")
  vim.cmd([[execute "normal! \<C-w>h"]])
  vim.cmd("diffthis")
end

lvim.normalize_mode['<leader>mc'] = ':lua compare_to_clipboard()<CR>'

-- Make leader+s+s to prepare CtrlSF search
lvim.keys.normal_mode['<leader>sF'] = '<cmd>CtrlSFPrompt<CR>'
lvim.keys.visual_mode['<leader>sF'] = '<cmd>CtrlSFVwordPath<CR>'

-- Keybindings for tests (using neotest), prefixed by <leader>t
lvim.keys.normal_mode['<leader>tt'] = "<cmd>lua require('neotest').run.run()<CR>"
lvim.keys.normal_mode['<leader>to'] = function()
  require('neotest').output_panel.open()
end
-- lvim.keys.normal_mode['<leader>ts'] = "<cmd>lua require('neotest').summary.toggle()<CR>"
lvim.keys.normal_mode['<leader>tD'] = function()
  -- require('neotest').run.run(vim.fn.expand('%:p:h'))
  require("neotest").run.run(vim.fn.expand("%"))
end
lvim.keys.normal_mode['<leader>tS'] = function()
  require('neotest').run.run({ suite = true })
end
lvim.keys.normal_mode['<leader>tw'] = function()
  require('neotest').watch.watch()
end

