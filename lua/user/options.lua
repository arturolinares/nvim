
vim.opt.scrolloff = 2;

-- Terminal
-- lvim.builtin.toggleterm.shell = 'fish'
require("toggleterm").setup {
  shell = "fish",
}

-- File Management
-- Auto-create parent directories (except for URIs "://")
vim.g.mkdir_parents = 1

-- Start git blame on col 80
vim.g.gitblame_virtual_text_column = 80

-- Disable alpha
lvim.builtin.alpha.active = false

-- Projects (disabled in favor of projrectmr)
-- lvim.builtin.project.patterns = { ">projects", ".root", ".git", "composer.json", "Makefile" }
-- lvim.builtin.project.scope_chdir = 'global';

lvim.builtin.nvimtree.setup.update_cwd = false
lvim.builtin.nvimtree.setup.sync_root_with_cwd = false;
-- Don't cut the filenames.
lvim.builtin.nvimtree.setup.view.adaptive_size = false;
-- floating tree
local HEIGHT_RATIO = 0.8 -- You can change this
local WIDTH_RATIO = 0.5  -- You can change this too
lvim.builtin.nvimtree.setup.view.float = {
  enable = true,
  open_win_config = function()
    local screen_w = vim.opt.columns:get()
    local screen_h = vim.opt.lines:get() - vim.opt.cmdheight:get()
    local window_w = screen_w * WIDTH_RATIO
    local window_h = screen_h * HEIGHT_RATIO
    local window_w_int = math.floor(window_w)
    local window_h_int = math.floor(window_h)
    local center_x = (screen_w - window_w) / 2
    local center_y = ((vim.opt.lines:get() - window_h) / 2)
        - vim.opt.cmdheight:get()
    return {
      border = 'rounded',
      relative = 'editor',
      row = center_y,
      col = center_x,
      width = window_w_int,
      height = window_h_int,
    }
  end
}
lvim.builtin.nvimtree.setup.view.width = function()
  return math.floor(vim.opt.columns:get() * WIDTH_RATIO)
end
-- lualine
-- lvim.builtin.lualine.sections.lualine_x = { "aerial" }

-- Use relative line numbers
vim.opt.relativenumber = true
-- Remove empty spaces at the end of lines on save
vim.cmd [[autocmd BufWritePre * %s/\s\+$//e]]
-- Use dark variants
vim.opt.background = 'dark';

vim.o.guifont = 'Maple Mono:h12.5';

vim.opt.shell = "/usr/bin/fish"

if vim.g.neovide then
  vim.g.neovide_transparency = 1
  vim.g.transparency = 1
  vim.opt.linespace = 1
  vim.g.neovide_scale_factor = 0.85

  vim.g.neovide_scroll_animation_length = 0.35
  -- vim.g.neovide_refresh_rate = 60
  vim.g.neovide_confirm_quit = true
  -- vim.g.neovide_input_macos_alt_is_meta = false
  -- blur
  vim.g.neovide_floating_blur_amount_x = 2.0
  vim.g.neovide_floating_blur_amount_y = 2.0
  vim.g.neovide_floating_blur = true
  vim.g.neovide_hide_mouse_when_typing = true


  vim.g.neovide_floating_shadow = true
  vim.g.neovide_floating_z_height = 10
  vim.g.neovide_light_angle_degrees = 45
  vim.g.neovide_light_radius = 5

  -- vim.keymap.set('n', '<C-S-Insert>', '"+P')
  -- clipboard
  vim.keymap.set('n', '<C-S-v>', '"+P')         -- Paste normal mode
  vim.keymap.set('v', '<C-S-v>', '"+P')         -- Paste visual mode
  vim.keymap.set('c', '<C-S-v>', '<C-R>+')      -- Paste command mode
  vim.keymap.set('i', '<C-S-v>', '<ESC>l"+Pli') -- Paste insert mode
  -- scale
  local change_scale_factor = function(delta)
    vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta
  end
  vim.keymap.set("n", "<C-+>", function()
    change_scale_factor(1.25)
  end)
  vim.keymap.set("n", "<C-->", function()
    change_scale_factor(1 / 1.25)
  end)
end

-- vim.opt.guifont = 'Maple Mono NL Regular';
-- LazyGit plugin options
vim.g.lazygit_floating_window_winblend = 0.9       -- transparency of floating window
vim.g.lazygit_floating_window_scaling_factor = 0.9 -- scaling factor for floating window
-- vim.g.lazygit_floating_window_border_chars = ['╭','─', '╮', '│', '╯','─', '╰', '│'] -- customize lazygit popup window border characters
vim.g.lazygit_floating_window_use_plenary = 1      -- use plenary.nvim to manage floating window if available
vim.g.lazygit_use_neovim_remote = 0                -- fallback to 0 if neovim-remote is not installed

vim.g.lazygit_use_custom_config_file_path = 0      -- config file path is evaluated if this value is 1
vim.g.lazygit_config_file_path = ''                -- custom config file path

-- ticket.vim configs
vim.g.auto_ticket = 1;
vim.g.auto_ticket_git_only = 1;
vim.g.ticket_black_list = { 'master', 'main' };
vim.g.auto_ticket_save = 1;

vim.opt.hlsearch = false
vim.opt.incsearch = true
-- vim.opt.scrolloff = 8
-- vim.opt.colorcolumn = "80"


-- Add workspace command and add sessions
local Workspace = require("projections.workspace")
vim.api.nvim_create_user_command("AddWorkspace", function()
  Workspace.add(vim.loop.cwd())
end, {})

local Session = require("projections.session")
vim.api.nvim_create_user_command("StoreProjectSession", function()
  Session.store(vim.loop.cwd())
end, {})

vim.api.nvim_create_user_command("RestoreProjectSession", function()
  Session.restore(vim.loop.cwd())
end, {})

local switcher = require("projections.switcher")
vim.api.nvim_create_user_command("SwitchProject", function(opts)
  switcher.switch(opts.args)
  require('dap.ext.vscode').load_launchjs()
end, { nargs = 1 })

-- lualine
local devcontainer_status = function()
  local build_status_last = require("devcontainer.status").find_build({ running = true })
  local status = ""
  if build_status_last then
    status = "🐳 "
        .. "["
        .. (build_status_last.current_step or "")
        .. "/"
        .. (build_status_last.step_count or "")
        .. "]"
        .. (build_status_last.progress and "(" .. build_status_last.progress .. "%%)" or "")
  end

  return status
end

local project_name_display = function()
  local projections_available, Session = pcall(require, 'projections.session')
  if projections_available then
    local info = Session.info(vim.loop.cwd())
    if info ~= nil then
      -- local session_file_path = tostring(info.path)
      -- local project_workspace_patterns = info.project.workspace.patterns
      -- local project_workspace_path = tostring(info.project.workspace)
      local project_name = info.project.name
      return '@' .. project_name
    end
  end
  return vim.fs.basename(vim.loop.cwd())
end

local lsp_status = function()
  if #vim.lsp.get_active_clients() == 0 then
    return ""
  end

  local lsp = vim.lsp.util.get_progress_messages()[1]
  if lsp then
    local name = lsp.name or ""
    local msg = lsp.message or ""
    local percentage = lsp.percentage or 0
    local title = lsp.title or ""
    return string.format(" %%<%s: %s (%s%%%%) ", name, title, percentage)
  end
  return ""
end

local project_and_devcontainer = function()
  return project_name_display() .. lsp_status() .. devcontainer_status()
end

vim.opt.showtabline = 0
vim.opt.editorconfig = true

lvim.builtin.lualine.sections.lualine_c = { { project_and_devcontainer } };

-- lvim.autocommands = {
--   {
--     "BufWinEnter", {
--     pattern = { "*.php", "*.yaml" },
--     callback = function()
--       -- DYI editorconfig
--       if vim.loop.cwd() == "/home/alinares/projects/sf-casts-last" then
--         vim.cmd [[setlocal tabstop=4 shiftwidth=4]]
--       end
--     end
--   },
--   }
-- }

-- codeium setup
table.insert(lvim.builtin.cmp.sources, { name = "codeium" })
lvim.builtin.cmp.formatting.source_names.codeium = "(Codeium)"
local default_format = lvim.builtin.cmp.formatting.format
lvim.builtin.cmp.formatting.format = function(entry, vim_item)
  vim_item = default_format(entry, vim_item)
  if entry.source.name == "codeium" then
    vim_item.kind = ""
    vim_item.kind_hl_group = "CmpItemKindCodeium"
  end
  return vim_item
end
vim.api.nvim_set_hl(0, "CmpItemKindCodeium", { fg = "#FDE030" })

-- Change the ft for twig files.
vim.api.nvim_command('autocmd BufNewFile,BufRead *.twig set filetype=html.twig')

local function get_class(with_namespace)
  -- Save current window view
  local win_view = vim.api.nvim_win_get_view(0)

  -- Save registers a and b
  local old_a = vim.api.nvim_get_reg(0, "+a")
  local old_b = vim.api.nvim_get_reg(0, "+b")

  -- Put namespace in register a
  vim.api.nvim_cmd("g/namespace/normal! W\"ayt;")

  -- Put class name in register b
  vim.api.nvim_cmd("g/^\\s*\\(abstract\\)*\\s*\\(interface\\|class\\)/normal! /(interface\\|class)<CR>W\"byiw")

  -- Get namespace and class from registers
  local namespace = vim.api.nvim_get_reg(0, "+a")
  local class = vim.api.nvim_get_reg(0, "+b")

  -- Restore registers a and b
  vim.api.nvim_set_reg(0, "+a", old_a)
  vim.api.nvim_set_reg(0, "+b", old_b)

  -- Restore window view
  vim.api.nvim_win_set_view(0, win_view)

  -- Return class with or without namespace
  return with_namespace and namespace .. "\\" .. class or class
end

-- Normal mode mappings
lvim.keys.normal_mode['<leader>yc'] = function()
  vim.api.nvim_set_reg(0, "*", get_class(false))
end

lvim.keys.normal_mode['<leader>ycn'] = function()
  vim.api.nvim_set_reg(0, "*", get_class(true))
end

