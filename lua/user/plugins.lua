lvim.plugins = {
  -- tmux navigation plugin
  { "christoomey/vim-tmux-navigator" },
  { 'rcarriga/nvim-notify' },
  { 'nvim-telescope/telescope-ui-select.nvim' },
  {
    "glepnir/template.nvim",
    config = function()
      require('template').setup({
        temp_dir = '~/.config/lvim/templates',
        author = 'Arturo Linares',
      })
    end
  },
  {
    "gnikdroy/projections.nvim",
    branch = "pre_release",
    config = function()
      require("projections").setup({
        show_preview = true,
        auto_restore = true,
        workspaces_file = "~/.config/lvim/workspaces.json",

        -- workspaces = {
        --   { "~/.config", { "config.lua" } },
        --   "~/projects",
        -- },
        patterns = { ".git", "composer.json", "packages.json" },
        store_hooks = {
          pre = function()
            -- nvim-tree
            local nvim_tree_present, api = pcall(require, "nvim-tree.api")
            if nvim_tree_present then api.tree.close() end
          end
        },
        restore_hooks = {
          post = function()
            -- dap
            require('dap.ext.vscode').load_launchjs()
            -- nvim-tree
            local nvim_tree_present, api = pcall(require, "nvim-tree.api")
            if nvim_tree_present then api.tree.close() end
          end
        }
      })

      -- Autostore session on VimExit
      local Session = require("projections.session")
      vim.api.nvim_create_autocmd({ 'VimLeavePre' }, {
        callback = function() Session.store(vim.loop.cwd()) end,
      })

      -- Switch to project if vim was started in a project dir
      local switcher = require("projections.switcher")
      vim.api.nvim_create_autocmd({ "VimEnter" }, {
        callback = function()
          if vim.fn.argc() == 0 then switcher.switch(vim.loop.cwd()) end
        end,
      })
    end
  },
  {
    "glepnir/template.nvim",
    config = function()
      require('template').setup({
        temp_dir = '~/.config/lvim/templates',
        author = 'Arturo Linares',
      })
    end
  },
  {
    "folke/todo-comments.nvim",
    config = function()
      -- Default keywords
      -- @see https://github.com/folke/todo-comments.nvim/blob/main/lua/todo-comments/config.lua
      local default_keywords = {
        FIX = {
          icon = " ", -- icon used for the sign, and in search results
          color = "error", -- can be a hex color, or a named color (see below)
          alt = { "FIXME", "BUG", "FIXIT", "ISSUE" }, -- a set of other keywords that all map to this FIX keywords
          -- signs = false, -- configure signs for some keywords individually
        },
        TODO = { icon = " ", color = "info", alt = { "todo" } },
        HACK = { icon = " ", color = "warning" },
        WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
        PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
        NOTE = { icon = "🗒️", color = "hint", alt = { "INFO" } },
        INFO = { icon = '💁', color = "info", alt = { "INFO" } },
        TEST = { icon = "⏲ ", color = "test", alt = { "TESTING", "PASSED", "FAILED" } },
      }
      -- @note Hola

      -- Custom keywords
      local keywords = {
        SEE = {
          icon = "",
          color = "info",
        },
      }

      keywords = vim.tbl_deep_extend("force", {}, default_keywords, keywords)
      -- Add lowercase versions of each keyword
      for key, val in pairs(keywords) do
        keywords[key:lower()] = val
      end

      require("todo-comments").setup({
        keywords = keywords,
        highlight = {
          pattern = [[.*[@]{1}<(KEYWORDS)\s*|.*<(KEYWORDS)\s*:]],
        },
        search = {
          -- pattern = [[[\\@]*\b(KEYWORDS)(\s|:)]],
        }
      })
    end
  },
  {
    "ThePrimeagen/refactoring.nvim",
    config = function()
      require('refactoring').setup()
    end
  },
  { 'mg979/vim-visual-multi' },
  { 'catppuccin/nvim' },
  {
    'rmehri01/onenord.nvim',
    config = function()
      -- hello
      require('onenord').setup({
        styles = {
          comments = 'italic',
          diagnostics = 'undercurl',
        }
      })
    end
  },
  { "mbbill/undotree" },
  { 'tpope/vim-fugitive' },
  { 'tpope/vim-surround' },
  {
    'stevearc/aerial.nvim',
    version = 'v1.4.0',
    config = function()
      require('aerial').setup({})
    end
  },
  {
    'ramojus/mellifluous.nvim',
    config = function()
      require("mellifluous").setup({
        color_set = "mellifluous"
      });
    end
  },
  { 'sainnhe/edge' },
  { 'kepano/flexoki-neovim', name = 'flexoki' },
  {
    "EdenEast/nightfox.nvim",
    config = function()
      require('nightfox').setup({
        options = {
          transparent = true,
          dim_inactive = true,
          styles = {
            comments = "italic",
          },
          inverse = {
            visual = true,
          }
        }
      });
    end
  },
  {
    'Exafunction/codeium.nvim',
    dependencies = {
      "nvim-lua/plenary.nvim",
      "hrsh7th/nvim-cmp",
    },
    config = function()
      require("codeium").setup({
        enable_chat = true,
        enable_index_service = true,
        -- enterprise_mode = true,
      })
    end
  },
  {
    'zbirenbaum/copilot.lua',
    enabled = false,
    config = function()
      require('copilot').setup({
        panel = {
          enabled = false,
        },
        suggestion = {
          enabled = false,
        },
        filetypes = {
          yaml = true,
          markdown = true,
        }
      })
    end
  },
  {
    'zbirenbaum/copilot-cmp',
    enabled = false,
    config = function()
      local has_words_before = function()
        if vim.api.nvim_buf_get_option(0, "buftype") == "prompt" then return false end
        local line, col = unpack(vim.api.nvim_win_get_cursor(0))
        return col ~= 0 and vim.api.nvim_buf_get_text(0, line - 1, 0, line - 1, col, {})[1]:match("^%s*$") == nil
      end
      local cmp = require('copilot_cmp')
      cmp.setup({
        mapping = {
          ["<Tab>"] = vim.schedule_wrap(function(fallback)
            if cmp.visible() and has_words_before() then
              cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
            else
              fallback()
            end
          end),
        },
      })
    end
  },
  { 'kdheepak/lazygit.nvim' },
  {
    'vladdoster/remember.nvim',
    config = function()
      require('remember');
    end
  },
  -- for java
  { "mfussenegger/nvim-jdtls" },
  {
    "rose-pine/neovim",
    name = "rose-pine",
    config = function()
      require('rose-pine').setup({
        variant = 'dawn',
      })
    end
  },
  { "tpope/vim-dadbod" },
  { "tpope/vim-rhubarb" },
  { "shumphrey/fugitive-gitlab.vim" },
  { "tommcdo/vim-fubitive" },
  -- temporary buffers
  {
    "m-demare/attempt.nvim",
    config = function()
      require("attempt").setup({
        dir = vim.fn.expand("~/.local/state/nvim.attempt"),
        autosave = true,
        list_buffers = true,
        ext_options = { "php", "js", "rs", "java", "html", "yaml", "sql" }
      })
    end
  },
  { "f-person/git-blame.nvim" },
  {
    "simrat39/symbols-outline.nvim",
    config = function()
      require("symbols-outline").setup {
        auto_close = true,
      }
    end
  },
  {
    "WhoIsSethDaniel/toggle-lsp-diagnostics.nvim",
    config = function()
      require('toggle_lsp_diagnostics').init()
    end
  },
  { 'dyng/ctrlsf.vim' },
  { 'projekt0n/github-nvim-theme' },
  {
    'johmsalas/text-case.nvim',
    config = function()
      require('textcase').setup {}
      require('telescope').load_extension('textcase')
      vim.api.nvim_set_keymap('n', 'ga.', '<cmd>TextCaseOpenTelescope<CR>', { desc = "Telescope" })
      vim.api.nvim_set_keymap('v', 'ga.', "<cmd>TextCaseOpenTelescope<CR>", { desc = "Telescope" })
    end
  },
  {
    "arturolinares/telescope-alternate.nvim",
    config = function()
      local drupalAlternatives = {
        { '[1]/[2]/[2].info.yml',              'Info',             true },
        { '[1]/[2]/[2].install',               'Install',          true },
        { '[1]/[2]/[2].deploy.php',            'Deploy Hooks',     true },
        { '[1]/[2]/[2].routing.yml',           'Routing',          true },
        { '[1]/[2]/[2].services.yml',          'Services',         true },
        { '[1]/[2]/[2].module',                'Module',           true },
        { '[1]/[2]/[2].links.action.yml',      'Local Actions',    true },
        { '[1]/[2]/[2].links.task.yml',        'Local Tasks',      true },
        { '[1]/[2]/[2].links.contextual.yml',  'Contextual Links', true },
        { '[1]/[2]/[2].libraries.yml',         'Library',          true },
        { '[1]/[2]/src/*Manager.php',          'PluginManager',    true },
        { '[1]/[2]/src/Entity/*.php',          'Entity',           true },
        { '[1]/[2]/src/Event/*.php',           'Event',            true },
        { '[1]/[2]/src/EventSubscriber/*.php', 'EventSubscriber',  true },
        { '[1]/[2]/src/Plugin/**/*.php',       'Plugin',           true },
        { '[1]/[2]/src/Form/**/*.php',         'Form',             true },
        { '[1]/[2]/src/Controller/*.php',      'Controller',       true },
      };
      require('telescope-alternate').setup({
        mappings = {
          { '(.*)/(.*)/(.*).info.yml',                drupalAlternatives },
          { '(.*)/(.*)/(.*).install.yml',             drupalAlternatives },
          { '(.*)/(.*)/(.*).services.yml',            drupalAlternatives },
          { '(.*)/(.*)/(.*).deploy.php',              drupalAlternatives },
          { '(.*)/(.*)/(.*).routing.yml',             drupalAlternatives },
          { '(.*)/(.*)/(.*).libraries.yml',           drupalAlternatives },
          { '(.*)/(.*)/(.*).links.action.yml',        drupalAlternatives },
          { '(.*)/(.*)/(.*).links.task.yml',          drupalAlternatives },
          { '(.*)/(.*)/(.*).links.contextual.yml',    drupalAlternatives },
          { '(.*)/(.*)/(.*).module',                  drupalAlternatives },
          { '(.*)/(.*)/(.*).install',                 drupalAlternatives },
          { '(.*)/(.*)/src/Entity/(.*).php',          drupalAlternatives },
          { '(.*)/(.*)/src/Event/(.*).php',           drupalAlternatives },
          { '(.*)/(.*)/src/EventSubscriber/(.*).php', drupalAlternatives },
          { '(.*)/(.*)/src/Controller/(.*).php',      drupalAlternatives },
          { '(.*)/(.*)/src/Plugin/*/*/(.*).php',      drupalAlternatives },
          { '(.*)/(.*)/src/Plugin/*/(.*).php',        drupalAlternatives },
          { '(.*)/(.*)/src/Form/*/(.*).php',          drupalAlternatives },
        },
      })
    end
  },
  {
    'ray-x/lsp_signature.nvim',
    config = function()
      require('lsp_signature').setup({
        hint_enable = true,
        floating_window = false,
      });
    end
  },
  {
    "gbprod/phpactor.nvim",
    -- commit = "9e9518a3e0a7f05facddfae1b5f80a37a8691f5b",
    -- run = require("phpactor.handler.update"), -- To install/update phpactor when installing this plugin
    config = function()
      require("phpactor").setup({
        install = {
          bin = "/usr/local/bin/phpactor",
          -- bin = "/home/alinares/bin/phpactor",
          -- branch = "main",
          path = vim.fn.stdpath("data") .. "/mason/phpactor",
          -- bin = vim.fn.stdpath("data") .. "/mason/bin/phpactor",
        },
        lspconfig = {
          enabled = false,
          options = {},
        }
      })
    end
  },
  {
    "arturolinares/hierarchy.nvim",
    config = function()
    end
  },
  { "moevis/base64.nvim" },
  {
    "norcalli/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup({ "css", "scss", "html", "javascript" }, {
        RGB = true,      -- #RGB hex codes
        RRGGBB = true,   -- #RRGGBB hex codes
        RRGGBBAA = true, -- #RRGGBBAA hex codes
        rgb_fn = true,   -- CSS rgb() and rgba() functions
        hsl_fn = true,   -- CSS hsl() and hsla() functions
        css = true,      -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
        css_fn = true,   -- Enable all CSS *functions*: rgb_fn, hsl_fn
      })
    end,
  },
  { "tpope/vim-abolish" },
  -- devcontainer support
  {
    url = 'https://codeberg.org/esensar/nvim-dev-container',
    config = function()
      require('devcontainer').setup({
        attach_mounts = {
          neovim_config = {
            enabled = true,
            options = { "readonly" }
          },
          neovim_data = {
            enabled = true,
            options = {}
          },
          -- Only useful if using neovim 0.8.0+
          neovim_state = {
            enabled = true,
            options = {}
          },
          lunarvim_config = {
            enabled = true,
            options = {}
          },
        },
      });
    end
  },
  {
    "nvim-neotest/neotest",
    lazy = false,
    dependencies = {
      "olimorris/neotest-phpunit",
      "nvim-neotest/nvim-nio",
      "nvim-lua/plenary.nvim",
      "antoinemadec/FixCursorHold.nvim",
      "nvim-treesitter/nvim-treesitter"
    },
    config = function()
      require("neotest").setup({
        adapters = {
          require("neotest-phpunit")({
            env = {
              XDEBUG_CONFIG = "idekey=neotest",
              IS_DDEV_PROJECT = "1",
            },
            -- if php is defined
            -- dap = require('dap').configurations.php[1],
            filter_dirs = { "vendor" },
            phpunit_cmd = function()
              -- if a file named .neotest.sh is found, use it as the command and exit early.
              if vim.fn.filereadable(".neotest.sh") == 1 then
                print("Using .neotest.sh")
                return { "./.neotest.sh" }
              end
              local phpunit = { "vendor/bin/phpunit" }
              -- find all phpunit.xml files under {docroot,web}/core and append to the phpnit command the paths, prefixed with -c each
              local cmd = "find */core -name phpunit.xml"
              local files = vim.fn.systemlist(cmd)
              for _, file in ipairs(files) do
                -- apend the -c option to the phpunit command
                table.insert(phpunit, "-c")
                -- append the path to the phpunit.xml file
                table.insert(phpunit, file)
              end
              -- Add the phpunit.xml as a configuration
              -- use an array of possible paths, and if found, add it to the command line.
              local paths = { 'phpunit.xml', 'docroot/core/phpunit.xml', 'web/core/phpunit.xml',
                'web/core/phpunit.xml.dist' }
              for _, path in ipairs(paths) do
                if vim.fn.filereadable(path) == 1 then
                  table.insert(phpunit, "-c")
                  table.insert(phpunit, path)
                end
              end
              -- If the file {docroot,web}/core/tests/boostrap.php exsits, configure it as bootstrap.
              if vim.fn.filereadable("docroot/core/tests/bootstrap.php") == 1 then
                table.insert(phpunit, "--bootstrap")
                table.insert(phpunit, "docroot/core/tests/bootstrap.php")
              end
              if vim.fn.filereadable("web/core/tests/bootstrap.php") == 1 then
                table.insert(phpunit, "--bootstrap")
                table.insert(phpunit, "web/core/tests/bootstrap.php")
              end
              return phpunit
            end,
          })
        },
      })
    end
  },
  { 'superDross/ticket.vim' },
  {
    'kevinhwang91/nvim-ufo',
    dependencies = { 'kevinhwang91/promise-async' },
    config = function()
      require('ufo').setup({
        provider_selector = function(bufnr, filetype, buftype)
          return { 'treesitter', 'indent' }
        end
      })
    end
  },
  {
    "karb94/neoscroll.nvim",
    event = "WinScrolled",
    config = function()
      require('neoscroll').setup({
        -- All these keys will be mapped to their corresponding default scrolling animation
        mappings = { '<C-u>', '<C-d>', '<C-b>', '<C-f>',
          '<C-y>', '<C-e>', 'zt', 'zz', 'zb' },
        hide_cursor = true,            -- Hide cursor while scrolling
        stop_eof = true,               -- Stop at <EOF> when scrolling downwards
        use_local_scrolloff = false,   -- Use the local scope of scrolloff instead of the global scope
        respect_scrolloff = false,     -- Stop scrolling when the cursor reaches the scrolloff margin of the file
        cursor_scrolls_alone = true,   -- The cursor will keep on scrolling even if the window cannot scroll further
        easing_function = 'quadratic', -- Default easing function
        pre_hook = nil,                -- Function to run before the scrolling animation starts
        post_hook = nil,               -- Function to run after the scrolling animation ends
      })
    end
  },
}

-- disable project
lvim.builtin.project.active = false;
-- codeium setup
table.insert(lvim.builtin.cmp.sources, { name = "codeium" })
lvim.builtin.cmp.formatting.source_names.codeium = "(Codeium)"
local default_format = lvim.builtin.cmp.formatting.format
lvim.builtin.cmp.formatting.format = function(entry, vim_item)
  vim_item = default_format(entry, vim_item)
  if entry.source.name == "codeium" then
    vim_item.kind = ""
    vim_item.kind_hl_group = "CmpItemKindCodeium"
  end
  return vim_item
end
vim.api.nvim_set_hl(0, "CmpItemKindCodeium", { fg = "#FDE030" })

-- obtained from the java quickstart
vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "jdtls" })
